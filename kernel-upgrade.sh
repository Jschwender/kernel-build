#!/bin/bash
#  Automatisierte Kernelaktualisierung für selbstkonfigurierte Kernel unter Debian/Ubuntu
#  erfordert libncurses5-dev kernel-package libssl-dev build-essential fakeroot bzip2 liblz4-tool (lz4) wget, internetverbindung
#
RECENT_ATOM_CONFIG='config-5.0.17-atom'   # Config-Datei muss vorliegen!
#VERSION_OVERRIDE='5.9.13'                # Kommentar entfernen für manuelles Steuern der neuen Version
Build=1

function myexit() { echo "$1 ENDE."; exit 1; }

finger_Version_Number() {
    if [ -z "$VERSION_OVERRIDE" ]
    then
	wget -O - https://www.kernel.org/finger_banner |grep "$1" |head -1|sed "s/.*of the Linux kernel is://g"  |awk '{print $1}'
    else
	echo "$VERSION_OVERRIDE"
    fi
}
#
#CONCURRENCY_LEVEL=$(getconf _NPROCESSORS_ONLN)         # alle installierten Prozessoren verwenden (entspricht  nproc --all )
CONCURRENCY_LEVEL=$(nproc)                              # ale verfügbaren Kerne verwenden
export CONCURRENCY_LEVEL
NewVERSION=$(finger_Version_Number stable)            # Neue Version (mainline|stable) verfügbar auf kernel.org
MAJ=$(echo $NewVERSION|awk -F'.' '{print $1}')          # Hauptversion des Kernels
KERNEL_DIR=linux-$NewVERSION
KERNEL_PACK=$KERNEL_DIR.tar.xz
KERNEL_SIGN=$KERNEL_DIR.tar.sign
echo "packaging $ImagePaket $HeaderPaket    LV:$LVERSION     NV:$NewVERSION"

update_Symlink() {
    find . -maxdepth 1 -type l -name linux -delete
    ln -s "$1" linux || exit 1
}

http_get_KernelPackage() {
    [ -f "${KERNEL_DIR}.tar" ] && return   # nichts zu tun wenn das Ding bereits entpackt vorliegt.
    ping -c 1 "www.kernel.org" >/dev/null || myexit "Keine Internetverbindung. ENDE"
    wget "https://www.kernel.org/pub/linux/kernel/v$MAJ.x/$KERNEL_PACK" || exit
    wget "https://www.kernel.org/pub/linux/kernel/v$MAJ.x/$KERNEL_SIGN" || exit
    ls -l "$KERNEL_PACK"
    xz -d "$KERNEL_PACK" || myexit "Entpacken von $KERNEL_PACK fehlgeschlagen"
    gpg2 --verify "$KERNEL_SIGN" "$KERNEL_DIR.tar" || myexit "Signaturcheck failed"
}
#
list_required_package() {
    for p in kernel-package libncurses5-dev libssl-dev build-essential fakeroot bzip2 liblz4-tool make flex libelf-dev xz-utils 
    do
    	    dpkg -l "$p" 2>/dev/null |grep "^ii" >/dev/null ||printf "$p "
    done
}
install_required_packages() {
    local Pakete=$(list_required_package)
    if [ -n "$Pakete" ]
    then apt install $Pakete
    else echo "Erforderlichen Pakete sind installiert: OK"
    fi
}
#
unpack_if_not_done() {
    #   Wenn es das Verzeichnis noch nicht gibt enpacken wir die TAR-Datei
    if [ ! -d ""$KERNEL_DIR"" ]
    then
        echo "unpacking  "$KERNEL_DIR"…"
        tar -xf ""$KERNEL_DIR".tar" || myexit "Entpacken von "$KERNEL_DIR".tar fehlgeschlagen"
    fi
}
#
build_kernelpackage() {
    local LVERSION="$1"
    local BUILD="$2"
    cd "$KERNEL_DIR" || myexit " cd ín das KernelVerzeichnis $KERNEL_DIR fehlgeschlagen"
    make menuconfig || myexit "Menuconfig fehlgeschlagen"
    #  Kernelpakete bauen, $3 könnte ARCH=x86 sein
    time make -j "$CONCURRENCY_LEVEL" $3 bindeb-pkg LOCALVERSION=-$LVERSION KDEB_PKGVERSION=$BUILD || myexit "Make fehlgeschlagen"
    ##time make-kpkg  --initrd --revision="$LVERSION" --rootcmd fakeroot kernel_image kernel_headers   # creates kernel-image + kernel-headers
    #make -j $(nproc) bindeb-pkg
       #  binary  kernel_headers   # creates ~-image ~-headers ~-docs ~-dbg
    cd .. || myexit "cd .. fehlgeschlagen"
}
#
install_KernelPakete() {
    dpkg -i $1  || myexit "Installation des Paketes  $1 fehlgeschlagen."
    apt update           # nochmal die Distribution aktualisieren
    apt -y upgrade
}
#
copy_current_config() {
    cp -f "/boot/config-$(uname -r)" "$KERNEL_DIR/.config" || myexit "kopieren fehlgeschlagen"
}
#
copy_atom_config() {
    cp -f $RECENT_ATOM_CONFIG "$KERNEL_DIR/.config"
}

#
http_get_KernelPackage || myexit "QuellTAR $KERNEL_PACK holen hat nicht funktiniert."
unpack_if_not_done
install_required_packages
update_Symlink "${KERNEL_DIR}"
[ -f "kernel-patch.sh" ] && source kernel-patch.sh
#

CPUTYPE=xeon
build_kernelpackage ${CPUTYPE} ${Build}
install_KernelPakete   "linux-image-${NewVERSION}-${CPUTYPE}_${Build}_amd64.deb linux-headers-${NewVERSION}-${CPUTYPE}_${Build}_amd64.deb"
#   ALTERNATIV:
#CPUTYPE=atom
#build_kernelpackage ${CPUTYPE} ${Build} ARCH=x86
#
#
