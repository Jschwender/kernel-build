#!/bin/bash
# Version 1.0.0
#  verwendet /usr/local/sbin/get-verified-tarball   Skript!!!
#  Automatisierte Kernelaktualisierung für selbstkonfigurierte Kernel unter Debian/Ubuntu
#  erfordert libncurses5-dev kernel-package libssl-dev build-essential fakeroot bzip2 liblz4-tool (lz4) wget, internetverbindung
#
#VERSION_OVERRIDE='6.7.9'                # Kommentar entfernen für manuelles Steuern der neuen Version
RECENT_ALT_CONFIG='config-6.6.10-xeon'
CPUTYPE='xeon'
Build=1
DL_DIR=/root/Downloads
get-verified-tarball $VERSION_OVERRIDE
ls -l $DL_DIR
VERSIONS=$(cat ${DL_DIR}/VERSION)
echo "$VERSIONS"
NewVERSION=$(echo  "$VERSIONS"|cut -d"	" -f2)
MAJ=$(echo  "$VERSIONS"|cut -d"	" -f1)
TARBALL=$(echo  "$VERSIONS"|cut -d"	" -f3)
. kernel-upgrade-lib
echo $NewVERSION $MAJ $TARBALL

mv ${TARBALL} /usr/src
#
unpack_if_not_done
install_required_packages
update_Symlink "${KERNEL_DIR}"
[ -f "kernel-patch.sh" ] && source kernel-patch.sh
#
copy_current_config

##TERM=xterm
build_kernelpackage ${CPUTYPE} ${Build} ARCH=x86
FV=${NewVERSION}-${CPUTYPE}_${NewVERSION}-${Build}
install_KernelPakete   "linux-image-${FV}_amd64.deb linux-headers-${FV}_amd64.deb linux-libc-dev_${NewVERSION}-${Build}_amd64.deb"
#
